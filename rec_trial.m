function rec_trial(trial)

global exp;

%% Save trial variables to lists
exp.CATdata.stime(trial)            = exp.t;                              % Stimulus presentation time

if ~isempty(exp.key)                                                        % If a keypress has been made
    exp.CATdata.key(trial)          = exp.key(1);                         % Which key has been pressed?
    exp.CATdata.keytime(trial)      = exp.keytime(1);                     % Keypress time
    exp.CATdata.RT(trial)           = exp.keytime(1) - exp.t;             % Time elapsed between cue onset (t) and time of key press (keytime)
    exp.CATdata.ACC(trial)          = exp.ACC;                            % 1 if correct; 0 if false; nan if no response
    exp.CATdata.theta(trial)        = exp.theta;           % Rotation of the normal arrows
    exp.CATdata.length(trial)       = exp.scale;           % Length/scale of line
    exp.CATdata.cat_RB(trial)       = exp.stimulus(4);
    exp.CATdata.cat_II(trial)       = exp.stimulus(5);
    else                                                                        % If no keypress, evything is nan
    exp.CATdata.mean_rotation(trial)= nan; 
    exp.CATdata.mean_length(trial)  = nan;
    exp.CATdata.cat_RB(trial)       = nan;
    exp.CATdata.cat_II(trial)       = nan;
    exp.CATdata.key(trial)          = nan;
    exp.CATdata.keytime(trial)      = nan;
    exp.CATdata.RT(trial)           = nan;
    exp.CATdata.ACC(trial)          = nan;  
end

exp.key = [];

%% Save results to file
result_file_name = [exp.results_filepath, sprintf('Results/CAT%03s_%03s_%i_%s.mat', exp.task_ver, exp.subj, exp.session, exp.version)];
save(result_file_name, 'exp');
