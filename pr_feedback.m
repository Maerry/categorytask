function ACC = pr_feedback(trial)

global exp

%% Determine correct answer
if exp.cat == 1
    cor_key = exp.nkey.one;
    fal_key = [exp.nkey.two, exp.nkey.three, exp.nkey.four];
elseif exp.cat == 2
    cor_key = exp.nkey.two;
    fal_key = [exp.nkey.one, exp.nkey.three, exp.nkey.four];
elseif exp.cat == 3
    cor_key = exp.nkey.three;
    fal_key = [exp.nkey.one, exp.nkey.two, exp.nkey.four];
elseif exp.cat == 4
    cor_key = exp.nkey.four;
    fal_key = [exp.nkey.one, exp.nkey.two, exp.nkey.three];
end

%% Determine accuracy and present feedback
if ~isempty(exp.key)
    drawpict(exp.buffer.stimulus)
    if exp.key == cor_key
        ACC = 1;
        exp.points = exp.points + 1;
        setforecolour(0, 1, 0);
        exp.msg1 = ['Correct! The category was ',num2str(exp.cat),'.'];
    elseif (exp.key ~= cor_key && any(fal_key==exp.key))
        ACC = 0;
        setforecolour(1, 0, 0);
        exp.msg1 = ['False! The category was ',num2str(exp.cat),'.'];
    else
       ACC = nan;
       setforecolour(1, 1, 1);
       exp.msg1 = 'Wrong key! (please use the number keys)';
    end
else
    ACC = nan;
    setforecolour(1, 1, 1);
    exp.msg1 = 'Too late!';
end

preparestring(exp.msg1, exp.buffer.stimulus, 0, -300);
drawpict(exp.buffer.stimulus);
wait(exp.times.feedback);
clearpict(exp.buffer.stimulus);
setforecolour(1, 1, 1);

end
