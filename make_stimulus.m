function make_stimulus

global exp;

%% prepare line stimulus 
exp.theta = exp.stimulus(1); % raw theta used for coordinates
exp.scale = pi*20 + exp.stimulus(3)*40; % total length of stimuli

exp.x1 = cos(exp.theta)*exp.scale;
exp.y1 = sin(exp.theta)*exp.scale;
exp.x2 = -exp.x1;
exp.y2 = -exp.y1;

%% prepare gabor patch
[~, ~, exp.G1] = gabor('theta', exp.theta, 'lambda', exp.scale/5, 'sigma', 40, ... 
                       'width', 500, 'height', 500, 'px', 0.5, 'py', 0.5);
exp.G1 = exp.G1 - min(min(exp.G1));
exp.G1 = exp.G1 / max(max(exp.G1));

%% get task version for feedback later
if strcmp(exp.task_ver, 'rb')
    exp.cat = exp.stimulus(4);
elseif strcmp(exp.task_ver, 'ii')
    exp.cat = exp.stimulus(5);
end

%% make the stimuli for buffer
if exp.isline
    cgpencol(1,1,1);
    cgpenwid(2);
    cgdraw(exp.x1, exp.y1, exp.x2, exp.y2)
    cgflip(0,0,0);
elseif exp.ispatch
    preparepict(exp.G1, exp.buffer.stimulus);
end

end