function category_task(category_version)

global exp;

exp.points = 0; % counter for points
exp.task_ver = category_version;
exp.isline = exp.version == 'A' || exp.version == 'B';
exp.ispatch = exp.version == 'C' || exp.version == 'D';

%% Run the task
init_category

%%% Give task instructions    
pr_instruction;

for trial = 1:exp.numb_of_trials.category  % exp.numb_of_trials.categrory = number of trials for rotation task
    clearpict(exp.buffer.stimulus);
    exp.stimulus = exp.trial_list(trial,:);
    init_trial(trial); % Clear buffers, prepare buffers, and initialize truth values  
    
    %%% Present stimulus
    exp.t = drawpict(exp.buffer.stimulus);
%     wait(exp.times.stimulus)
%     drawpict(exp.buffer.prompt)
    [exp.key, exp.keytime, exp.n] = waitkeydown(exp.times.cue);                 % Wait for response and get identity and time of pressed key
    
    %%% Present feedback
    exp.ACC = pr_feedback(trial);
    
    rec_trial(trial);   % Record trial information
    
    if mod(trial, exp.summary_feedback_trial.category) == 0 && trial ~= 1
        pr_summary_feedback(trial);
        exp.points = 0;
    end
end

%% Say goodbye
pr_money_bonus;
stop_cogent;
