function pr_money_bonus    

global exp

%%% Calculate task bonus
received_points = nansum(exp.CATdata.ACC);
min_points      = exp.numb_of_trials.category / 4;   % If subject always choses the worst option
if received_points < min_points
    min_points = received_points;                        
end
max_points      = exp.numb_of_trials.category;   % If subject always choses the best option

bonus = (received_points - min_points) / (max_points - min_points) * ...
    exp.bonus.max_session/6;

%%% Store task bonus (to calculate total bonus at the end)
if any(strcmp(exp.version, {'A', 'C'}))
    exp.bonus.task2_run1 = round(bonus, 2);
elseif any(strcmp(exp.version, {'B', 'D'}))
    exp.bonus.task2_run2 = round(bonus, 2);
end

m11 = 'Great job!';
m12 = ['You achieved a bonus of $', num2str(round(bonus, 2)), ' in this task!'];
m13 = '(This bonus is based on your performance and added';
m14 = 'to your regular payment.)';
m15 = 'Press space when you want to move on to your next task.';

clearpict(exp.buffer.instruction);
preparestring(m11, exp.buffer.instruction, 0, 290); %'Great job!';
preparestring(m12, exp.buffer.instruction, 0, 200); %['You achieved a bonus of $', num2str(round(bonus, 2)), ' in this task!'];
preparestring(m13, exp.buffer.instruction, 0, 0); %'(This bonus is based on your performance and added';
preparestring(m14, exp.buffer.instruction, 0,-50); %'to your regular payment.)';
drawpict(exp.buffer.instruction);
waitkeydown(inf, 71);

preparestring(m15, exp.buffer.instruction, 0, -250); %'Press space when are ready for your next task.';
drawpict(exp.buffer.instruction);
waitkeydown(inf, 71);

end
