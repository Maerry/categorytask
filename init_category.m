function init_category

global exp; % declares experiment variable

%% Configure Cogent
% Configure display & keyboard - window size 0 for troubleshooting
config_display(1, 3, [0,0,0], [1 1 1], 'Helvetica', 40, 10, 0);   % Configure display (0 = window mode; 640x480, 800x600, 1024x768, 1152x864, 1280x1024, 1600x1200; [1 1 1] = white background; grey text; fontname; fontsize; nbuffers)
config_keyboard;    % sets up background

%% Set Variables
exp.wdir                = cd;                                                       % Working directory

% Fix durations for everything in the experiment
exp.times.stimulus      =   500;                                                    % Duration of stimulus presentation
exp.times.cue           =  7000;                                                    % Duration of key press cue
exp.times.feedback      =   800;                                                    % Max duration of feedback 
exp.times.iti           = ones(1, exp.numb_of_trials.category);                     % Inter-trial interval [0.3 0.7] with mean 0.5
while mean(exp.times.iti) > .501 || mean(exp.times.iti) < .499
   exp.times.iti = 0.3 + 0.4 * rand(1, exp.numb_of_trials.category);
end 

% Clear all buffers
for buffer = 1:6
    clearpict(buffer)
end
% Name buffers
exp.buffer.stimulus                     = 1;
exp.buffer.fixation                     = 2;
exp.buffer.feedback                     = 3;
exp.buffer.blank                        = 4;
exp.buffer.instruction                  = 5;
exp.buffer.message                      = 6;
% Load fixation cross into buffer
preparestring('+', exp.buffer.fixation);

% Initialize log stuff
exp.CATdata.ACC             = zeros(1, exp.numb_of_trials.category);              % idk what this is
exp.CATdata.RT              = [];                                                 % reaction time
exp.CATdata.stime           = [];                                                 % Initialize (stimulus presentation time?)
exp.CATdata.key             = [];                                                 % Initialize for data saving
exp.CATdata.keytime         = [];                                                 % honestly probably odn't have to worry about this

% Set keys to select arrows and stimuli
exp.nkey.one          = 28;     % key index 2 corresonds to '1' 
exp.nkey.two          = 29;     % key index 3 corresponds to '2'
exp.nkey.three        = 30;
exp.nkey.four         = 31;

%% Get trial list in order (pseudo-randomize categories; insert summary feedback)
% Load the stimuli
if any(strcmp(exp.version, {'A', 'C'}))
    exp.stim_list = csvread('category_trials1');
else
    exp.stim_list = csvread('category_trials2');
end
% What category (1:4) is presented in which trial?
exp.trial_categories = zeros(exp.numb_of_trials.category, 1);
for chunk = 1:8:exp.numb_of_trials.category
    exp.trial_categories(chunk:chunk+7) = datasample([1:4, 1:4], 8, 'replace', false);
end
exp.trial_categories = exp.trial_categories(1:exp.numb_of_trials.category);
% Create a list of trials in the order given by exp.trial_categories
exp.trial_list = zeros(exp.numb_of_trials.category, 6);
for category = 1:4
    if strcmp(exp.task_ver, 'rb')
        cat_trials = find(exp.stim_list(:,4) == category);
    elseif strcmp(exp.task_ver, 'ii')
        cat_trials = find(exp.stim_list(:,5) == category);
    end
    exp.trial_list(exp.trial_categories == category, 1:5) = ...
        exp.stim_list(cat_trials(1:sum(exp.trial_categories == category)),:);
end
% Add summary feedbacks
exp.summary_feedback_trial.category = ceil(exp.numb_of_trials.category / exp.number_of_summary_feedbacks);
feedback_trials = exp.summary_feedback_trial.category:exp.summary_feedback_trial.category:exp.numb_of_trials.category;
exp.trial_list(feedback_trials,6) = 1;
exp.trial_list(end,6) = 1;

%% Try to start cogent (else, wait for enter - to get matlab window)
try
    start_cogent;
catch
    input('Please press enter to continue')
    start_cogent;
end

end

