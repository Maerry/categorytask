
% list of information of 160 stimuli: theta, factor, category (II), category (RB)

num_per_half_cat = 240/8;

%% set-up
% list_of_stim = list of arrays, length 160
% stim_info = [raw_theta, theta, scale, cat.RB, cat.II]
% raw_theta = actual angle, used for coordinates
% theta = tana(abs(tan(raw_theta))), for rotation category
% len = length of line (excluding base length), percentages of 2*pi
% cat.II = category of line if info-int. task, either 2 or 4 groups
% cat.RB = category of line if rule-based task, either 2 or 4 groups

%% building stimuli in RB 1, II 1; points are between lines y = 4/x, y = pi, x = pi/4)
list_11 = zeros(num_per_half_cat,5); 
for i=1:num_per_half_cat
    len = pi+pi*rand();
    raw_theta = 2*pi*rand();
    while ~(atan(abs(tan(raw_theta))) > pi/4 && atan(abs(tan(raw_theta))) < pi/2 && atan(abs(tan(raw_theta))) < len/4)
        raw_theta = 2*pi*rand();
    end
    theta = atan(abs(tan(raw_theta)));
    stim_info = [raw_theta, theta, len, 1, 1];
    list_11(i,:) = stim_info;
end
list_11;

%% building stimuli in RB 1, II 2; points are between lines y = 4/x, y = pi, x = pi/2)
list_12 = zeros(num_per_half_cat,5);  
for i=1:num_per_half_cat
    len = pi+pi*rand();
    raw_theta = 2*pi*rand();
    while ~(atan(abs(tan(raw_theta))) > pi/4 && atan(abs(tan(raw_theta))) < pi/2 && atan(abs(tan(raw_theta))) > len/4)
        raw_theta = 2*pi*rand();
    end
    theta = atan(abs(tan(raw_theta)));
    stim_info = [raw_theta, theta, len, 1, 2];
    list_12(i,:) = stim_info;
end
list_12;

%% building stimuli in RB 2, II 2; points are between lines y = -x/4+pi/2, y = pi, x = pi/2)
list_22 = zeros(num_per_half_cat,5);  
for i=1:num_per_half_cat
    len = pi*rand();
    raw_theta = 2*pi*rand();
    while ~(atan(abs(tan(raw_theta))) > pi/4 && atan(abs(tan(raw_theta))) < pi/2 && atan(abs(tan(raw_theta))) > -len/4 + pi/2)
        raw_theta = 2*pi*rand();
    end
    theta = atan(abs(tan(raw_theta)));
    stim_info = [raw_theta, theta, len, 2, 2];
    list_22(i,:) = stim_info;
end
list_22;

%% building stimuli in RB 2, II 3; points are between lines y = -x/4+pi/2, y = pi, x = pi/2)
list_23 = zeros(num_per_half_cat,5);  
for i=1:num_per_half_cat
    len = pi*rand();
    raw_theta = 2*pi*rand();
    while ~(atan(abs(tan(raw_theta))) > pi/4 && atan(abs(tan(raw_theta))) < pi/2 && atan(abs(tan(raw_theta))) < -len/4 + pi/2)
        raw_theta = 2*pi*rand();
    end
    theta = atan(abs(tan(raw_theta)));
    stim_info = [raw_theta, theta, len, 2, 3];
    list_23(i,:) = stim_info;
end
list_23;

%% building stimuli in RB 3, II 3; points are between lines y = x/4, y = pi, x = pi/2)
list_33 = zeros(num_per_half_cat,5);  
for i=1:num_per_half_cat
    len = pi*rand();
    raw_theta = 2*pi*rand();
    while ~(atan(abs(tan(raw_theta))) > 0 && atan(abs(tan(raw_theta))) < pi/4 && atan(abs(tan(raw_theta))) > len/4)
        raw_theta = 2*pi*rand();
    end
    theta = atan(abs(tan(raw_theta)));
    stim_info = [raw_theta, theta, len, 3, 3];
    list_33(i,:) = stim_info;
end
list_33;

%% building stimuli in RB 3, II 4; points are between lines y = x/4, y = pi, x = pi/2)
list_34 = zeros(num_per_half_cat,5);  
for i=1:num_per_half_cat
    len = pi*rand();
    raw_theta = 2*pi*rand();
    while ~(atan(abs(tan(raw_theta))) > 0 && atan(abs(tan(raw_theta))) < pi/4 && atan(abs(tan(raw_theta))) < len/4)
        raw_theta = 2*pi*rand();
    end
    theta = atan(abs(tan(raw_theta)));
    stim_info = [raw_theta, theta, len, 3, 4];
    list_34(i,:) = stim_info;
end
list_34;

%% building stimuli in RB 3, II 4; points are between lines y = -x/4+pi/2, y = pi, x = pi/2)
list_44 = zeros(num_per_half_cat,5);  
for i=1:num_per_half_cat
    len = pi+pi*rand();
    raw_theta = 2*pi*rand();
    while ~(atan(abs(tan(raw_theta))) > 0 && atan(abs(tan(raw_theta))) < pi/4 && atan(abs(tan(raw_theta))) < -len/4+pi/2)
        raw_theta = 2*pi*rand();
    end
    theta = atan(abs(tan(raw_theta)));
    stim_info = [raw_theta, theta, len, 4, 4];
    list_44(i,:) = stim_info;
end
list_44;

%% building stimuli in RB 4, II 1; points are between lines y = -x/4+pi/2, y = pi, x = pi/2)
list_41 = zeros(num_per_half_cat,5);  
for i=1:num_per_half_cat
    len = pi+pi*rand();
    raw_theta = 2*pi*rand();
    while ~(atan(abs(tan(raw_theta))) > 0 && atan(abs(tan(raw_theta))) < pi/4 && atan(abs(tan(raw_theta))) > -len/4+pi/2)
        raw_theta = 2*pi*rand();
    end
    theta = atan(abs(tan(raw_theta)));
    stim_info = [raw_theta, theta, len, 4, 1];
    list_41(i,:) = stim_info;
end
list_41;

%% plotting ALL the things
plot([pi,pi],[0,pi/2],'--b');
hold on
plot([0,2*pi],[pi/4,pi/4],'--b'); % vertical, horizontal division (RB categories)
hold on
plot([0,2*pi],[pi/2,0],'-.r'); % diagonal divisions (II categories)
hold on
plot([0,2*pi],[0,pi/2],'-.r');
hold on

% plotting RB 1 (black), II 1 (o)
plot(list_11(:,3),list_11(:,2),'ko');
hold on

% plotting RB 1 (black), II 2 (+)
plot(list_12(:,3),list_12(:,2),'k+');
hold on

% plotting RB 2 (red), II 2 (+)
plot(list_22(:,3),list_22(:,2),'r+');
hold on

% plotting RB 2 (red), II 3 (*)
plot(list_23(:,3),list_23(:,2),'r*');
hold on

% plotting RB 3 (green), II 3 (*)
plot(list_33(:,3),list_33(:,2),'g*');
hold on

% plotting RB 3 (green), II 4 (x)
plot(list_34(:,3),list_34(:,2),'gx');
hold on

% plotting RB 4 (blue), II 4 (x)
plot(list_44(:,3),list_44(:,2),'bx');
hold on

% plotting RB 4 (blue), II 1 (o)
plot(list_41(:,3),list_41(:,2),'bo');
hold on

xlabel('Length of line');
ylabel('Orientation of line (0 to 90 degrees)');

list_of_stim = vertcat(list_11, list_12, list_22, list_23, list_33, list_34, list_44, list_41);
csvwrite('category_trials', list_of_stim)