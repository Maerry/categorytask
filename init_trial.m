function init_trial(trial)

global exp;

%% Clear keys and prepare stimulus
clearkeys;
make_stimulus

%% Initialize truth values
exp.ACC                 = nan;
exp.key                 = [];
exp.keytime             = [];
exp.n                   = 0;
exp.catch_ACC           = nan;

end
