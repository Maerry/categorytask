function pr_instruction

%start_cogent;

global exp;

upper_end = 300;

%% Write messages
% Determine if long or short version of the instructions
if any(strcmp(exp.version, {'A', 'C'}))     % First time in a session: Explain task
    long_version = true;
else
    long_version = false;
end

if long_version
    if exp.isline
        m11 = 'In the following task, you will see lines of';
        m12 = 'different lengths and orientations. For example:';
        m13 = 'You will see one line at a time and decide to which';
        m14 = 'of 4 categories it belongs. You will get feedback';
        m15 = 'after every decision to help you learn the categories.';
        m21 = 'You might see this line, for example:';
    elseif exp.ispatch
        m11 = 'In the following task, you will see Gabor patches of';
        m12 = 'different thickness and orientations. For example:';
        m13 = 'You will see one patch at a time and decide to which';
        m14 = 'of 4 categories it belongs. You will get feedback';
        m15 = 'after every decision to help you learn the categories.';
        m21 = 'You might see this patch, for example:';
    end
    m22 = 'You will press 1, 2, 3, or 4 (in the number line) to';
    m23 = 'make a guess to which category it belongs.';
    m23b= 'Please put 4 fingers on these 4 buttons now. Use the same';
    m23c= 'fingers throughout the task to make sure you hit the right buttons.';
    m24 = 'The feedback will tell you which was the right category';
    m25 = 'and whether you answered correctly.';
    m26 = 'Try to improve until you get every trial right!';
else                                  % Second time in a session: Just remind
    m11 = 'You will now play the category task again.';
    m12 = 'The categories are exactly as before.';
    m13 = 'Remember: Press 1, 2, 3, or 4 (in the number line) to';
    m14 = 'select a category.';
    m15 = '';
end
m31 = 'Do you have any questions? Raise your hand if you do.';
m32 = 'Press the space key when you are ready to start.';

% slide 1
clearkeys;
clearpict(1)    % refreshes the display buffer for instructions
preparestring(m11, 1, 0, upper_end);
preparestring(m12, 1, 0, upper_end - 50);
if long_version
    if exp.isline
        cgpencol(1,1,1);
        cgpenwid(2);
        cgdraw(-350, -20, -300, 20)
        cgdraw(-250, 200, -150, -200)
        cgdraw(-100, -120, 0, 120)
        cgdraw(50, 40, 80, -40)
        cgdraw(130, 130, 350, -80)
    elseif exp.ispatch
        loadpict('stimuli/GaborMiddle.png', 1,-170, 60);
        loadpict('stimuli/GaborSmall.png',  1,   0, 60);
        loadpict('stimuli/GaborBig.png',    1, 170, 60);
    end
end
drawpict(1);
waitkeydown(inf, 71);
preparestring(m13, 1, 0, -upper_end + 80);
preparestring(m14, 1, 0, -upper_end + 30);
preparestring(m15, 1, 0, -upper_end - 20);
drawpict(1);
waitkeydown(inf, 71);

if long_version
    % slide 2
    clearpict(1);
    preparestring(m21, 1, 0, upper_end);
    if exp.isline
        cgdraw(120, 250, -120, -50);
    elseif exp.ispatch
        loadpict('stimuli/GaborBig.png', 1, 0, 110);
    end
    drawpict(1);
    waitkeydown(inf, 71);
    preparestring(m22, 1, 0, -100);
    preparestring(m23, 1, 0, -130);
    preparestring(m23b,1, 0, -160);
    preparestring(m23c,1, 0, -190);
    drawpict(1);
    waitkeydown(inf, 71);
    preparestring(m24, 1, 0, -240);
    preparestring(m25, 1, 0, -280);
    preparestring(m26, 1, 0, -320);
    drawpict(1);
    waitkeydown(inf, 71);
end

% slide 3
clearpict(1);
preparestring(m31, 1, 0,  50);
preparestring(m32, 1, 0, -50);
drawpict(1);
waitkeydown(inf, 71)
clearpict(1);

end