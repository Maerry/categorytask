This is a read-me file for the .png and .csv files for category_trials.

-= For the .png file =-

The blue dashed lines delineate the boundaries for rule-based (RB) categories, 
and the red dashed lines for information-integration (II) categories. 

There are 4 quadrants for each category type. Because the II category boundaries 
are just the RB categories rotated clockwise by 45 degrees, this gives rise to 8 
subquadrants. That is, 4 of these subquadrants have the same RB and II categories, 
while the other four have different RB and II categories. 

There are 160 points that vary by length and orientation (i.e. the degree of the 
incline). Graphically these points vary by color and shape. The different colors 
represent the different RB categories, and the different shapes represent the 
different II categories. There are 8 permutations of colors and shapes, with each 
permutation representing a subquadrant. For each permutation, there are 20 
different stimuli. 

For the actual task, the matrix will be shuffled so that the stimuli will be 
presented in random order. 

-= For the .csv file =-

If you look at the category_trials.csv, you will see a 160x5 matrix of values. Each 
row represents one stimuli, and each column stands for some property of the stimuli. 
The properties from left to right are: 
- raw_theta (between 0 and 2pi, used for drawing the stimuli), 
- theta (between 0 and pi/2, used for category-sorting), 
- length/scale (between 0 and 2*pi, used for drawing the line and also category-sorting), 
- RB category (from 1 to 4), 
- and II category (from 1 to 4). 
