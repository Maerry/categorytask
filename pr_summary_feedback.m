function pr_summary_feedback(trial)

global exp

%% Write and display the message
m1 = ['You got ', num2str(exp.points), ' of the last ', num2str(exp.summary_feedback_trial.category), ' trials right!'];
m2 = 'Press space to resume the task.';

clearpict(exp.buffer.message)
preparestring(m1, exp.buffer.message, 0, 200)
preparestring(m2, exp.buffer.message, 0,-200)
drawpict(exp.buffer.message)

waitkeydown(inf, 71);
